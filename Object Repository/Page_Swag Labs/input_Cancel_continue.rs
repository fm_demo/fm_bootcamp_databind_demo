<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Cancel_continue</name>
   <tag></tag>
   <elementGuidId>c58e23c2-8359-4fbd-8bd6-d33750256642</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='continue']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#continue</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ead1eb86-78c1-403a-b9cf-3c0512564c69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>73a163d8-5e5b-4312-8e3c-6d5070bd0aa8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn btn_primary cart_button btn_action</value>
      <webElementGuid>7ca8a367-8587-47cf-9232-11a1eb7b2e60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>f693bf2d-89f1-489c-a5e1-9c00d0f4795b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>2d6becc5-c02e-4ed3-a66e-2227f1e62387</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>1dd3a698-dd26-42c2-a79a-f08de5ae3a23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>0fe9084d-1b85-437f-8209-1f0f9b81632e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;continue&quot;)</value>
      <webElementGuid>0f483562-9774-4206-94eb-316394eb59fd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='continue']</value>
      <webElementGuid>097bfbb6-b33c-49a1-8409-fb7a2b9aaf51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div[2]/input</value>
      <webElementGuid>2e43a5a9-660a-44b5-9c88-c3d3b104f998</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>2a1247a2-b67c-4adf-b8f6-d74ab489da2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'continue' and @name = 'continue']</value>
      <webElementGuid>95f64853-6a6d-483d-b431-f8ee3314b30c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
